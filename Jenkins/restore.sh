#!/usr/bin/env bash

sudo /etc/init.d/jenkins stop
tar xzvf jenkins_backup.tar.gz
sudo cp -R jenkins-backup/* /var/lib/jenkins/
sudo chown jenkins:jenkins -R /var/lib/jenkins/
sudo /etc/init.d/jenkins start